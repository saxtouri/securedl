from django.apps import AppConfig


class SecuredlappConfig(AppConfig):
    name = 'securedlapp'
