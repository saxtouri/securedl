import os.path

from django.http import HttpResponse
from securedlapp.error import DLError
from securedlapp.logic import (access_expired, access_granted, download_lookup,
                               log_download)


# Create your views here.
def dl(request, dl_uuid):
    download = download_lookup(dl_uuid)
    if not download:
        return DLError(401, "Αυτός ο σύνδεσμος δεν υπάρχει.").redirect()
    elif access_expired(download):
        log_download(download, "410 Expired")
        return DLError(410, "Ο σύνδεσμος έχει απενεργοποιηθεί.").redirect()
    elif not access_granted(request, download):
        log_download(download, "403 Forbidden")
        return DLError(
            403, "Δεν επιτρέπεται η πρόσβαση από αυτή τη συσκευή ή τοποθεσία."
        ).redirect()

    _, filename = os.path.split(download.file)
    uuid_str = f'.{download.uuid}.'
    if uuid_str in filename:
        filename = '.'.join(filename.split(uuid_str))

    r = HttpResponse(content_type='application/force-download')
    r['Content-Disposition'] = f'attachment; filename={filename}'
    r['Cache-Control'] = 'private'
    # Apache
    # r['X-Sendfile'] = path_to_file
    # Nginx
    r['X-Accel-Redirect'] = download.file

    log_download(download, "Success")
    return r
