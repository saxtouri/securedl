from django.urls import path

from securedlapp import views

urlpatterns = [
    path('dl/<uuid:dl_uuid>/', views.dl, name='file_download'),
]
