import logging
from uuid import uuid4

from django.utils import timezone
from ipware import get_client_ip

from securedlapp.models import Download, DownloadLog


def log_download(dl, msg='Intialize'):
    return DownloadLog.objects.create(dl=dl, msg=msg[:2048])


def download_lookup(dl_uuid):
    try:
        return Download.objects.get(uuid=dl_uuid)
    except Download.DoesNotExist:
        return None


def get_device_id(request, dl):
    session_key = f'{dl.uuid}:device_id'
    return request.session.get(session_key)


def set_device_id(request, dl):
    session_key = f'{dl.uuid}:device_id'
    request.session[session_key] = f'{dl.device_id}'
    log_download(dl, f'Set device_id {dl.device_id}')


def set_ip(dl, ip):
    dl.ip = ip
    dl.save()
    log_download(dl, f'Set IP {ip}')


def access_expired(dl):
    return dl.expires_at < timezone.now()


def access_granted(request, dl):
    if access_expired(dl):
        log_download(dl, "Expired")
        return False

    ip, _ = get_client_ip(request)
    if not dl.ip:
        set_ip(dl, ip)
    ips_match = ip and (f'{dl.ip}' == ip)

    device_id = get_device_id(request, dl)
    if not dl.device_id:
        if not ips_match:
            log_download(dl, "No IP match and no device_id match")
            return False
        dl.device_id = uuid4()
        dl.save()
        set_device_id(request, dl)
    elif ips_match and not device_id:
        set_device_id(request, dl)
    device_match = device_id and (f'{dl.device_id}' == device_id)

    if ips_match and not device_match:
        set_device_id(request, dl)
    elif device_match and not ips_match:
        set_ip(dl, ip)
    return ips_match or device_match
