from django.contrib import admin

from securedlapp.models import Download, DownloadLog, User

# Register your models here.
admin.site.register(User)


@admin.register(Download)
class DownloadAdmin(admin.ModelAdmin):
    list_display = (
        "uuid", "user", "file", "created_at", "expires_at", "ip", "device_id")


@admin.register(DownloadLog)
class DownloadLogAdmin(admin.ModelAdmin):
    list_display = ("dl", "msg", "timestamp")
