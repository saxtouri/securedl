from logging import getLogger

from django.shortcuts import redirect

from securedl.settings import ERROR_REDIRECT_URL

# from urllib.parse import urlencode



logger = getLogger(__name__)


class DLError(Exception):
    status, msg = 500, ''

    def __init__(self, status=500, msg='Error in Request'):
        self.status = status
        self.msg = msg

    def redirect(self):
        print('hello')
        url = f'{ERROR_REDIRECT_URL}?msg={self.msg}'
        return redirect(to=url)
