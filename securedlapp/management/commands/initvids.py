import json
import logging
import os
from shutil import copyfile

from django.core.management.base import BaseCommand, CommandError
from securedlapp.logic import access_expired
from securedlapp.models import Download
from securedlapp.tags import add_mp4_tags

logger = logging.getLogger('cli')


class Command(BaseCommand):

    def handle(self, *args, **options):
        suffix = '.mp4'
        for dl in Download.objects.all():
            logger.info(f'\n{dl}')
            if access_expired(dl):
                logger.info('-- expired')
                continue
            elif not dl.file.endswith(suffix):
                logger.info(f'File {dl.file} does not end to {suffix}, ignore')
                continue
            source_path = dl.file
            dir_path, file_name = os.path.split(source_path)
            name = file_name[:-len(suffix)]
            uuid_str = f'{dl.uuid}'
            if name.endswith(uuid_str):
                logger.info(f'File "{file_name}"" already connected')
                continue

            target_name = f'{name}.{uuid_str}{suffix}'
            target_path = os.path.join(dir_path, target_name)
            logger.info(f'Copy {source_path} --> {target_path}')
            copyfile(source_path, target_path)

            user_str = f'{dl.user.name} <{dl.user.email}>'
            cmt = f'For {user_str} (licenced copy - exclussive use)'
            new_tags = {
                '©cmt': cmt,
                'comm': cmt,
                'licence': cmt,
            }
            logger.info('Add licencing tags: {}'.format(
                json.dumps(new_tags, indent=2, ensure_ascii=False)))
            add_mp4_tags(target_path, new_tags)

            logger.info(f'Update download with new file')
            dl.file = target_path
            dl.save()
