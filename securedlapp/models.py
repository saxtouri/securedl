import os
from datetime import timedelta
from uuid import uuid4

from django.db import models
from django.utils import timezone
from securedl.settings import VIDEO_DIR


class User(models.Model):
    name = models.CharField(max_length=128, null=False)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=64, null=True)

    def __str__(self):
        return f'{self.name} <{self.email}>'


class Download(models.Model):

    def now_plus_days(days=7):
        return timezone.now() + timedelta(days=7)

    uuid = models.UUIDField(default=uuid4, editable=False, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    file = models.FilePathField(path=os.path.abspath(VIDEO_DIR), null=False)
    created_at = models.DateTimeField(default=timezone.now)
    expires_at = models.DateTimeField(default=now_plus_days)
    ip = models.GenericIPAddressField(null=True, blank=True)
    device_id = models.UUIDField(null=True, blank=True)

    class Meta:
        unique_together = ('user', 'file')

    def __str__(self):
        _, fname = os.path.split(self.file)
        if f'{self.uuid}' in fname:
            fname, _ = fname.split(f'{self.uuid}')
            fname = fname[:-1]
        return f'{self.user} {fname} {self.uuid}'


class DownloadLog(models.Model):
    dl = models.ForeignKey(Download, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=timezone.now)
    msg = models.CharField(max_length=2048, null=False)
