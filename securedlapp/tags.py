from mutagen.mp4 import MP4


def add_mp4_tags(media_file, tags):
    medium = MP4(media_file)
    medium.update(tags)
    medium.save()
